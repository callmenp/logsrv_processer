<?php
class LogDBAgent{
    private $sqls;
    private $db;
    function __construct(){
        $config=Config::getInstance();
        $dbc=$config->getSection('db');
        $this->db=new PDO($dbc['driver'] .':dbname='.$dbc['dbname'] .';host='.$dbc['host'],$dbc['username'] ,$dbc['password']);
        $this->db->exec("SET NAMES 'utf8';");
    }
    function save($target, $data){
        if(!$this->sqls[$target]){
            $placeholder=substr(str_repeat('?,',count($data)),0,-1);
            $this->sqls[$target]=$this->db->prepare("insert into $target values($placeholder)");
        }
        $sql=$this->sqls[$target];
        $result=$sql->execute($data);
        if(!$result){
            var_dump($sql->errorInfo());
        }
        return $result;
    }
}
