<?php
class LogFile{
    function __construct(){
        $config=Config::getInstance();
        $fileconfig=$config->getSection('log_file');
        $this->fileNameFormat=$fileconfig['full_name_format'];
    }
    function buildFileName($time){
        $format=preg_replace('/\\\%\\\/','',preg_replace("/(.)/",'\\\$1',$this->fileNameFormat));
        return $time->format($format);
    }
    function getLogs($time){
        $fullFileName=$this->buildFileName($time);
        if(!file_exists($fullFileName)){
            echo $this->fileNameFormat;
            die("日志文件不存在: $fullFileName");
        }
        $contents=file_get_contents($fullFileName);
        if(false===$contents){
            die("日志文件无法读取: $fullFileName");
        }
        return $contents;
    }
}
