<?php
class LogParser{
    function parse($line){
        $nginxLog=json_decode($line);
        if(!$nginxLog){
            echo '无效的line';
            echo $line;
            return false;
        }
        $data=array();
        $data[]=$nginxLog->remote_addrx;
        $data[]=$nginxLog->time;
        $data[]=$nginxLog->http_user_agent;
        $data[]="";
        $data[]="";
        preg_match("/^(GET|POST) (.*) (HTTP\/1.1|HTTP\/0.9|HTTP\/1.1)$/",$nginxLog->request,$arr);
        if("GET"==$arr[1]){
            $url=$arr[2];
            $urlInfo=parse_url($url);
            if(!$urlInfo){
                return false;
            }
            if(!isset($urlInfo['query'])){
                echo '没有合法的query';
                return false;
            }
            $query=$urlInfo['query'];
        }elseif("POST"==$arr[1]){
            $query=$nginxLog->request_body;
        }else{
            echo "不支持的http方法";
            return false;
        }
        $params=array();
        $a=parse_str(urldecode($query),$params);
        if(!$params['target'] or !$params['data']){
            return false;
        }
        $params['data']=json_decode($params['data']);
        $request=$params;
        if(!is_array($request['data'])){
            echo "无效的data";
            return false;
        }
        $target=$request['target'];

        $max_i=count($request['data']);
        for($i=0;$i<$max_i;$i++){
            $request['data'][$i]=array_merge(array(NULL),$data,$request['data'][$i]);
        }
        return $request;
    }
}
