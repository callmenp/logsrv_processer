<?php
class LogProcesser{
    function __construct(){
        $this->logSrc=new LogFile();
        $this->parser=new LogParser();
        $this->storage=new LogDBAgent();
    }
    /**
     * 处理指定时间的日志
     * @param time:DateTime
     */
    function doProcess($time){
        $logs=$this->logSrc->getLogs($time);
        $logLines=explode("\n",$logs);
        $max_i=count($logLines)-1;
        for($i=0;$i<$max_i;$i++){
            $this->processLine($logLines[$i]);
        }
    }
    /**
     * 处理一行日志
     * @param line:String
     */
    function processLine($line){
        $data=$this->parser->parse($line);
        if(false===$data){
            echo "本行处理失败： $line";
            return;
        }
        foreach($data['data'] as $record){
            $this->storage->save($data['target'],$record);
        }
    }
}
