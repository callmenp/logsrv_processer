<?php
class Config{
    static private $_instance=null;
    private $_config=array();
    private function __construct(){
    }
    static public function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance=new Config();
        }
        return self::$_instance;
    }
    public function getSection($section){
        if(!$this->_config){
            $this->_config=parse_ini_file(__dir__."/config.ini",true);
        }
        return (isset($this->_config[$section]))?$this->_config[$section] : false;
    }
}
