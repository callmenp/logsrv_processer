<?php
/**
 * Generated by PHPUnit_SkeletonGenerator on 2015-03-01 at 22:39:32.
 */
class LogParserTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var LogParser
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new LogParser;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @covers LogParser::parse
     * @dataProvider nginxlogProvider
     */
    public function testParse($line)
    {
        $a=$this->object->parse($line);
        var_dump($a);
    }
    public function nginxlogProvider(){
    return array(
        array('{"remote_addrx":"192.168.1.1","time":"2015-02-28T10:33:11+08:00","status":"200","request":"GET /write?target=self_swf_log_v1&data='.urlencode('[[11,33],[11,22]]').' HTTP/1.1","http_user_agent":"curl/7.38.0" }')
    );
    }
}
